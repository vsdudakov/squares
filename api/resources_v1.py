from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.models import create_api_key

from tastypie.bundle import Bundle
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.exceptions import BadRequest

from django.db import models
from django.conf.urls import url
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login
from django.conf import settings

from squares.models import Square, WorkPlace, Settings


models.signals.post_save.connect(create_api_key, sender=get_user_model())


class BaseModelResource(ModelResource):
    pass


class UserResource(BaseModelResource):

    def get_object_list(self, request):
        return get_user_model().objects.filter(is_active=True)

    class Meta:
        object_class = get_user_model()
        resource_name = 'UserResource'
        excludes = ['email', 'password', 'is_superuser']
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']


class WorkPlaceResource(BaseModelResource):
    owner = fields.ForeignKey(UserResource, 'owner')
    users = fields.ManyToManyField(UserResource, 'users')

    def get_object_list(self, request):
        return WorkPlace.objects.filter(users=request.user)

    class Meta:
        object_class = WorkPlace
        resource_name = 'WorkPlaceResource'
        authentication = ApiKeyAuthentication()
        authorization = Authorization()


class SquareResource(BaseModelResource):
    owner = fields.ForeignKey(UserResource, 'owner')
    workplace = fields.ForeignKey(WorkPlaceResource, 'workplace')

    def get_object_list(self, request):
        if request.workplace is not None:
            return Square.objects.filter(workplace=request.workplace)
        else:
            return Square.objects.none()

    class Meta:
        object_class = Square
        resource_name = 'SquareResource'
        authentication = ApiKeyAuthentication()
        authorization = Authorization()


class SettingsResource(BaseModelResource):

    def get_object_list(self, request):
        return Settings.objects.all()

    class Meta:
        object_class = Settings
        resource_name = 'SettingsResource'
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
