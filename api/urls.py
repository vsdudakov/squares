from tastypie.api import Api

from django.conf.urls import patterns, include, url

from api import resources_v1


v1_api = Api(api_name='v1')
v1_api.register(resources_v1.UserResource())
v1_api.register(resources_v1.WorkPlaceResource())
v1_api.register(resources_v1.SquareResource())
v1_api.register(resources_v1.SettingsResource())


urlpatterns = patterns('',
    url(r'', include(v1_api.urls)),
)
