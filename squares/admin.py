from django.contrib import admin

from squares.models import Square, WorkPlace, Settings

admin.site.register(Square)
admin.site.register(WorkPlace)
admin.site.register(Settings)
