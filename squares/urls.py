from django.conf.urls import patterns, include, url
from django.contrib import admin

from squares.views import IndexView, WorkPlaceView, LoginView, LogoutView


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^workplace/$', WorkPlaceView.as_view(), name='workplace'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

    url(r'^files/uploader/$', 'squares.views.files_uploader', name='files-uploader'),
    url(r'^files/browser/$', 'squares.views.files_browser', name='files-browser'),

    url(r'^api/', include('api.urls')),
    url(r'^api/doc/', include('tastypie_swagger.urls', namespace='tastypie_swagger')),

    url(r'^admin/', include(admin.site.urls)),
)
