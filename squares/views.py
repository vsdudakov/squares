import os
import json

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout, login
from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView, RedirectView
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import HttpResponse

from squares.forms import LoginForm, UploadFileForm


class IndexView(TemplateView):
    template_name = 'index.html'

    def dispatch(self, request, *args, **kwargs):
        return redirect(reverse_lazy('workplace'))


class WorkPlaceView(TemplateView):
    template_name = 'workplace.html'

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(WorkPlaceView, self).dispatch(*args, **kwargs)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = reverse_lazy('workplace')

    def get_success_url(self):
        success_url = self.request.GET.get('next', None)
        if success_url is not None:
            return success_url
        return self.success_url

    def form_valid(self, form):
        login(self.request, form.user)
        return super(LoginView, self).form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(self.success_url)
        return super(LoginView, self).dispatch(request, *args, **kwargs)


class LogoutView(RedirectView):
    url = reverse_lazy('index')

    def get_redirect_url(self):
        logout(self.request)
        return super(LogoutView, self).get_redirect_url()

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(LogoutView, self).dispatch(*args, **kwargs)


@csrf_exempt
@login_required(login_url=reverse_lazy('login'))
def files_browser(request):
    pass


@csrf_exempt
@login_required(login_url=reverse_lazy('login'))
def files_uploader(request):
    def get_images_path(workplace, filename, root=settings.MEDIA_ROOT):
        try:
            root = '/squares/' + root
            os.makedirs(os.path.join(root, 'uploads', str(workplace.pk)))
        except Exception:
            pass
        return os.path.join(root, 'uploads', str(workplace.pk), filename)

    def handle_uploaded_file(workplace, f):
        path = get_images_path(workplace, f.name)
        with open(path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        return get_images_path(workplace, f.name, root=settings.MEDIA_URL)

    if request.method == 'POST':
        path = ''
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            path = handle_uploaded_file(request.workplace, request.FILES['upload'])
    return HttpResponse(path, content_type="application/json")
