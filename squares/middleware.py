from squares.models import WorkPlace


class WorkPlaceMiddleware(object):

    def process_request(self, request):
        workplace = None
        if request.user.is_authenticated():
            workplace_pk = request.GET.get('workplace_pk', request.session.get('workplace_pk', None))
            if workplace_pk is not None:
                workplace = WorkPlace.objects.filter(users=request.user, pk=workplace_pk).first()
            else:
                workplace = WorkPlace.objects.filter(users=request.user).first()
            if workplace is not None:
                request.session['workplace_pk'] = workplace.pk
        request.workplace = workplace
