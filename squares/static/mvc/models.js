var UserModel = Backbone.Model.extend({
    urlRoot: apiPrefix + '/api/v1/UserResource/',
    defaults: {
    }
});


var WorkPlaceModel = Backbone.Model.extend({
    urlRoot: apiPrefix + '/api/v1/WorkPlaceResource/',
    defaults: {
        'owner': null,
        'users': null,
        'name': '',
        'background_image': '/static/img/wallpaper.jpg'
    }
});


var SquaresModel = Backbone.Model.extend({
    urlRoot: apiPrefix + '/api/v1/SquareResource/',
    defaults: {
        'owner': null,
        'workplace': null,

        'top': 100,
        'left': 50,
        'min_width': 450,
        'min_height': 500,
        'width': 300,
        'height': 300,
        'z_index': 1,
        'background_color': '#E8E8E8',

        'is_deleted': false,
        'is_collapsed': false,
        'is_expanded': false,
        'is_done': false,

        'title': 'new square',
        'data': '',

        'title_edit_mode': false,
        'data_edit_mode': false
    }
});
