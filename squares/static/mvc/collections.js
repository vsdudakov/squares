var UserCollection = Backbone.Collection.extend({
    url: apiPrefix + '/api/v1/UserResource/',
    model: UserModel
});


var WorkPlaceCollection = Backbone.Collection.extend({
    url: apiPrefix + '/api/v1/WorkPlaceResource/',
    model: WorkPlaceModel
});


var SquaresCollection = Backbone.Collection.extend({
    url: apiPrefix + '/api/v1/SquareResource/',
    model: SquaresModel
});