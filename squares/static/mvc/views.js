var Views = { };    


var MenuView = Backbone.View.extend({
    el: $("#right-menu"),

    template: _.template($('#item').html()),

    events: {
        "click button.item": "collapseOrExpand",
    },

    initialize: function (workplace) {
        this.workplace = workplace;
    },

    getModelFromEvent: function(e) {
        //e.preventDefault();
        var cid = $(e.currentTarget).data("id");
        return this.collection.get(cid);
    },

    collapseOrExpand: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({is_collapsed: !model.get('is_collapsed')});
        model.save();
    },

    render: function () {
        var obj = this;
        obj.$el.empty();
        obj.collection.each(function (model) {
            if (!model.get('is_deleted')) {
                obj.$el.append(obj.template({item: model}));
            }
        });
    }
});


var SquaresView = Backbone.View.extend({
    el: $("#main"),
    collection: new SquaresCollection(),
    template: _.template($('#square').html()),
    events: {
        "click span.action-collapse": "collapse",
        "click span.action-remove": "remove",
        "mouseup .draggable-handler": "draggableStop",
        "mouseup .ui-resizable-handle": "resizableStop",
        "click .panel-default": "onTop",
        "dblclick .square-title": "clickTitle",
        "dblclick .square-data": "clickData",
        "click .action-title-save ": "saveTitle",
        "click .action-data-save": "saveData"
    },

    initialize: function (workplace) {
        this.workplace = workplace;
        this.menuView = new MenuView({ collection: this.collection, workplace: this.workplace });
        this.collection.on('change', this.render, this);
        this.collection.on('add', this.render, this);
    },

    getModelFromEvent: function(e) {
        //e.preventDefault();
        var cid = $(e.currentTarget).parents(".square").data("id");
        return this.collection.get(cid);
    },

    draggableStop: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({
            top: parseInt(e.pageY), 
            left: parseInt(e.pageX)
        });
        model.save();
    },

    resizableStop: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({
            width: parseInt($(e.currentTarget).parents('.panel').width()),
            height: parseInt($(e.currentTarget).parents('.panel').height())
        });
        model.save();
        $('body').css( 'cursor', 'default' );
    },

    onTop: function (e) {
        // var model = this.getModelFromEvent(e);
        // var z_index = _.max(this.collection.pluck('z_index'));
        // model.set({z_index: z_index + 1});
        // model.save();
    },

    clickTitle: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({title_edit_mode: true});
    },

    clickData: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({data_edit_mode: true});
    },

    saveTitle: function (e){
        var model = this.getModelFromEvent(e);
        var title = $(e.currentTarget).parent().find('input').val();
        model.set({title: title, title_edit_mode: false});
        model.save()
        return false;
    },

    saveData: function (e){
        var model = this.getModelFromEvent(e);
        var id = $(e.currentTarget).parent().find('textarea').attr('id');
        var data = CKEDITOR.instances[id].getData();
        model.set({data: data, data_edit_mode: false});
        model.save()
        return false;
    },

    collapse: function (e) {
        var model = this.getModelFromEvent(e);
        model.set({is_collapsed: true});
        model.save();
    },

    remove: function (e) {
        var model = this.getModelFromEvent(e);
        if (confirm('Are you sure you want to delete this square?')) {
            model.set({is_deleted: true});
            model.save();
        }
    },

    render: function () {
        var obj = this;
        $('#spin-loader').show();
        obj.$el.empty();
        this.collection.each(function (model) {
            if (!model.get('is_deleted') && !model.get('is_collapsed')) {
                obj.$el.append(obj.template({square: model}));
                obj.$el.find(".draggable" ).draggable({
                    handle: ".draggable-handler",
                    cursor: "move",
                    opacity: 0.35
                    //containment: "parent",
                });
                obj.$el.find(".resizable" ).resizable();
                var textarea = obj.$el.find("#textarea-" + model.get('id'));
                var id = textarea.attr('id');
                if (typeof id != 'undefined') {
                    var editor = CKEDITOR.replace(id, {
                        resize_enabled: false,
                        //filebrowserBrowseUrl: filebrowserBrowseUrl,
                        filebrowserUploadUrl: filebrowserUploadUrl,
                        enterMode: Number(2)
                    });
                }
            }
        });
        $('#spin-loader').hide();
        this.menuView.setElement($("#right-menu"));
        this.menuView.render();
    }
});


var WorkPlaceView = Backbone.View.extend({
    el: $("body"),
    collection: new WorkPlaceCollection(),
    template: _.template($('#workplace').html()),
    events: {
        "click button.add-square": "addSquare" 
    },

    initialize: function() {
        this.collection.fetch({async:false});
        this.workplace = this.getWorkPlace();
        this.squaresView = new SquaresView({ workplace: this.workplace });
    },

    getWorkPlace: function() {
        return this.collection.get(apiPrefix + '/api/v1/WorkPlaceResource/' + currentWorkplacePk + '/');
    },

    addSquare: function(e) {
        this.squaresView.collection.create({owner: this.workplace.get('owner'), workplace: this.workplace.id});
    },

    render: function () {
        this.$el.find('#container').html(this.template({workplace: this.workplace}));
        var obj = this;
        obj.squaresView.collection.fetch({
            success: function() {
                obj.squaresView.setElement($("#main"));
                obj.squaresView.render();
            }
        });
    }
});


var SettingsView = Backbone.View.extend({
    el: $("#container"),

    template: _.template($('#settings').html()),

    render: function () {
        this.$el.html(this.template());
    }
});


var ProfileView = Backbone.View.extend({
    el: $("#container"),

    template: _.template($('#profile').html()),

    render: function () {
        this.$el.html(this.template());
    }
});


Views = { 
    workplace: new WorkPlaceView(),
    settings: new SettingsView(),
    profile: new ProfileView() 
};
