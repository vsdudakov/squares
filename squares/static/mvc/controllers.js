var Controller = Backbone.Router.extend({
    routes: {
        "": "workplace",
        "!/": "workplace",
        "!/settings": "settings",
        "!/profile": "profile"
    },

    workplace: function () {
        if (Backbone.Tastypie.apiKey.key && Views.workplace != null) {
            $(".se-pre-con").fadeIn("slow");
            Views.workplace.render();
            $(".se-pre-con").fadeOut("slow");
        }
    },

    settings: function () {
        if (Backbone.Tastypie.apiKey.key && Views.settings != null) {
            Views.settings.render();
        }
    },

    profile: function () {
        if (Backbone.Tastypie.apiKey.key && Views.profile != null) {
            Views.profile.render();
        }
    }

});


var controller = new Controller(); // Создаём контроллер


Backbone.history.start();  // Запускаем HTML5 History push 