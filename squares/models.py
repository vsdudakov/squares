from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="%(app_label)s_%(class)s_owner")

    class Meta:
        abstract = True


class WorkPlace(BaseModel):
    name = models.CharField(max_length=255)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    background_image = models.CharField(max_length=255, default="/static/img/wallpaper.jpg")

    class Meta:
        verbose_name = ugettext_lazy(u'WorkPlace')
        verbose_name_plural = ugettext_lazy(u'WorkPlaces')

    def __unicode__(self):
        return u'%s' % self.name


class BaseSquare(BaseModel):
    COLOR_DEFAULT = '#E8E8E8'
    COLORS = (
        (COLOR_DEFAULT, 'Default'),
    )

    workplace = models.ForeignKey(WorkPlace, related_name="%(app_label)s_%(class)s_workplace")

    top = models.IntegerField(default=100)
    left = models.IntegerField(default=50)
    min_width = models.IntegerField(default=450)
    min_height = models.IntegerField(default=500)
    width = models.IntegerField(default=300)
    height = models.IntegerField(default=300)
    z_index = models.IntegerField(default=1)
    background_color = models.CharField(max_length=7, choices=COLORS, default=COLOR_DEFAULT)
    is_done = models.BooleanField(default=False)

    is_deleted = models.BooleanField(default=False)
    is_collapsed = models.BooleanField(default=False)
    is_expanded = models.BooleanField(default=False)

    title = models.CharField(max_length=255, default='new square')

    class Meta:
        abstract = True

    def __unicode__(self):
        return u'%s' % self.title


class Square(BaseSquare):
    data = models.TextField(default='')

    class Meta:
        verbose_name = ugettext_lazy(u'Square')
        verbose_name_plural = ugettext_lazy(u'Squares')


class Settings(BaseModel):
    
    class Meta:
        verbose_name = ugettext_lazy(u'Setting')
        verbose_name_plural = ugettext_lazy(u'Settings')

