#-*- encoding: utf-8 -*-
from squares.forms import WorkplaceForm
from django.conf import settings


def workplace(request):
    kwargs = {}
    if request.user.is_authenticated():
        kwargs['workplace'] = WorkplaceForm(request)
    kwargs['project'] = 'Happy Squares'
    kwargs['url_api_prefix'] = getattr(settings, 'URL_API_PREFIX', '')
    return kwargs
