from django import forms
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy

from squares.models import WorkPlace


class WorkplaceForm(forms.Form):
    workplace = forms.ModelChoiceField(queryset=WorkPlace.objects.none(), empty_label=None)

    def __init__(self, request, *args, **kwargs):
        super(WorkplaceForm, self).__init__(*args, **kwargs)
        self.fields['workplace'].queryset = WorkPlace.objects.filter(users=request.user)
        self.fields['workplace'].initial = request.workplace


class LoginForm(forms.Form):
    error_messages = {
        'invalid': ugettext_lazy(u'Username or password is not correct'),
    }

    username = forms.CharField(max_length=16, label=ugettext_lazy(u'Username'))
    password = forms.CharField(max_length=16, widget=forms.PasswordInput(), label=ugettext_lazy(u'Password'))
    user = None

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['placeholder'] = ugettext_lazy(u'Username')
        self.fields['password'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['placeholder'] = ugettext_lazy(u'Password')

    def clean(self):
        self.cleaned_data = super(LoginForm, self).clean()
        if 'password' in self.cleaned_data and 'username' in self.cleaned_data:
            self.user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
            if not self.user:
                raise forms.ValidationError(self.error_messages['invalid'])
        return self.cleaned_data


class UploadFileForm(forms.Form):
    upload = forms.FileField()