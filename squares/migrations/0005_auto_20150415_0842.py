# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squares', '0004_workplace_background_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='square',
            name='is_done',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='square',
            name='data',
            field=models.TextField(default=b''),
        ),
    ]
