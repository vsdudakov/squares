# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squares', '0003_workplace_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='workplace',
            name='background_image',
            field=models.CharField(default=b'/static/img/wallpaper.jpg', max_length=255),
            preserve_default=True,
        ),
    ]
