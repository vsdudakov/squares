# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('owner', models.ForeignKey(related_name=b'squares_settings_owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Square',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('top', models.IntegerField(default=0)),
                ('left', models.IntegerField(default=0)),
                ('min_width', models.IntegerField(default=100)),
                ('min_height', models.IntegerField(default=200)),
                ('width', models.IntegerField(default=100)),
                ('height', models.IntegerField(default=200)),
                ('z_index', models.IntegerField(default=1)),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_collapsed', models.BooleanField(default=False)),
                ('is_expanded', models.BooleanField(default=False)),
                ('title', models.CharField(default=b'', max_length=255)),
                ('data', models.TextField(default=b'{}')),
                ('owner', models.ForeignKey(related_name=b'squares_square_owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WorkPlace',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('owner', models.ForeignKey(related_name=b'squares_workplace_owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='square',
            name='workplace',
            field=models.ForeignKey(related_name=b'squares_square_workplace', to='squares.WorkPlace'),
            preserve_default=True,
        ),
    ]
