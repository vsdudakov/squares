# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squares', '0005_auto_20150415_0842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='square',
            name='min_height',
            field=models.IntegerField(default=500),
        ),
        migrations.AlterField(
            model_name='square',
            name='min_width',
            field=models.IntegerField(default=450),
        ),
    ]
