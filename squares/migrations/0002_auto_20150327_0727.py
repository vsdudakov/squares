# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squares', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='square',
            name='background_color',
            field=models.CharField(default=b'#E8E8E8', max_length=7, choices=[(b'#E8E8E8', b'Default')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='square',
            name='height',
            field=models.IntegerField(default=300),
        ),
        migrations.AlterField(
            model_name='square',
            name='left',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='square',
            name='min_height',
            field=models.IntegerField(default=300),
        ),
        migrations.AlterField(
            model_name='square',
            name='min_width',
            field=models.IntegerField(default=300),
        ),
        migrations.AlterField(
            model_name='square',
            name='title',
            field=models.CharField(default=b'new square', max_length=255),
        ),
        migrations.AlterField(
            model_name='square',
            name='top',
            field=models.IntegerField(default=100),
        ),
        migrations.AlterField(
            model_name='square',
            name='width',
            field=models.IntegerField(default=300),
        ),
    ]
