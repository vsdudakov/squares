#-*- encoding: utf-8 -*-
import fabric
import os

SITES_PATH = os.path.join('/home/users/v/vsdudakov/', 'django')
VIRTUALS_PATH = os.path.join('/home/users/v/vsdudakov/', 'virtuals') 

fabric.api.env.roledefs['production'] = ['managity.com']


def production_env():
    #fabric.api.env.user = 'vsdudakov'
    fabric.api.env.project_root = os.path.join(SITES_PATH, 'squares')
    fabric.api.env.shell = '/bin/bash -c'
    fabric.api.env.python = os.path.join(VIRTUALS_PATH, 'squares_v', 'bin', 'python')
    fabric.api.env.pip = os.path.join(VIRTUALS_PATH, 'squares_v', 'bin', 'pip')
    fabric.api.env.always_use_pty = False


def deploy():
    with fabric.api.cd(fabric.api.env.project_root):
        fabric.api.run('git pull origin master')
        fabric.api.run('{pip} install -r requirements.txt'.format(pip=fabric.api.env.pip))
        fabric.api.run('{python} manage.py collectstatic'.format(python=fabric.api.env.python))
        fabric.api.run('{python} manage.py migrate'.format(python=fabric.api.env.python))


@fabric.api.roles('production')
def prod():
    production_env()
    deploy()
