from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = patterns('',
    url(r'^', include('squares.urls')),
)

if settings.DEBUG:
    #urlpatterns += tuple(static(settings.STATIC_URL, document_root=settings.STATIC_ROOT, show_indexes=True))
    urlpatterns += tuple(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT, show_indexes=True))